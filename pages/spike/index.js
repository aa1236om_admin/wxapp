﻿const App = getApp();
const wxParse = require("../../utils/wxParse.js");
var order = ['red', 'yellow', 'blue', 'green', 'red'];
function timestampToTime(timestamp) {
	var date = new Date(timestamp * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
	var Y = date.getFullYear() + '/';
	var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '/';
	var D = date.getDate() + ' ';
	var h = date.getHours()+2 + ':0:0';

	return Y+M+D+h;
}
 
/* 毫秒级倒计时 */
function countdown(that,total_micro_second) {
   // 渲染倒计时时钟
   that.setData({
     clock:dateformat(total_micro_second)
   });
 
   if (total_micro_second <= 0) {
	 total_micro_second=0;
     that.setData({
       clock:"已经截止",
	   stock_num:0
     });
     // timeout则跳出递归
     return ;
   }  
   setTimeout(function(){
    // 放在最后--
    total_micro_second -= 10;
    countdown(that,total_micro_second);
  }
  ,10)
}
 
// 时间格式化输出，如3:25:19 86。每10ms都会调用一次
function dateformat(micro_second) {
   // 秒数
   var second = Math.floor(micro_second / 1000);
   // 小时位
   var hr = Math.floor(second / 3600);
   // 分钟位
   var min = Math.floor((second - hr * 3600) / 60);
   // 秒位
  var sec = (second - hr * 3600 - min * 60);// equal to => var sec = second % 60;
  // 毫秒位，保留2位
  var micro_sec = Math.floor((micro_second % 1000) / 10);
  return hr + ":" + min + ":" + sec;
}
Page({

  /**
   * 页面的初始数据
   */
  data: {

    indicatorDots: true, // 是否显示面板指示点
    autoplay: true, // 是否自动切换
    interval: 3000, // 自动切换时间间隔
    duration: 800, // 滑动动画时长

    currentIndex: 1, // 轮播图指针
    floorstatus: false, // 返回顶部
    showView: true, // 显示商品规格

    detail: {}, // 商品详情信息
    goods_price: 0, // 商品价格
    stock_num: 0, // 库存数量

    goods_num: 1, // 商品数量
    item_sku_id: 0, // 规格id
    cart_total_num: 0, // 购物车商品总数量
    specData: {}, // 多规格信息
	clock: '',
    // 分享按钮组件
    share: {
      show: false,
      cancelWithMask: true,
      cancelText: '关闭',
      actions: [{
        name: '生成商品海报',
        className: 'action-class',
        loading: false
      }, {
        name: '发送给朋友',
        openType: 'share'
      }],
      // 商品海报
      showPopup: false,
    },

  },

  // 记录规格的数组
  goods_spec_arr: [],

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(e) {
    let _this = this,
      scene = App.getSceneData(e);
    // 商品id
    _this.data.item_id = e.item_id ? e.item_id : scene.gid;
    // 获取商品信息
    _this.getGoodsDetail();
	var date  = new Date()
	var timestamp = Date.parse(new Date());  
    timestamp = timestamp / 1000; 
	var frontOneHour =  timestampToTime(timestamp);
	frontOneHour = Date.parse(frontOneHour) / 1000;
	var h = frontOneHour - timestamp;
	countdown(this,h * 1000);
  },

  /**
   * 获取商品信息
   */
  getGoodsDetail: function() {
    let _this = this;
    App._get('item/detail', {
      item_id: _this.data.item_id,
    }, function(result) {

      // 初始化商品详情数据
      let data = _this.initGoodsDetailData(result.data);
	 
      _this.setData(data);
    });
  },

  /**
   * 初始化商品详情数据
   */
  initGoodsDetailData: function(data) {
	   
    let _this = this;
    // 富文本转码
    if (data.detail.goods_content.length > 0) {
      wxParse.wxParse('content', 'html', data.detail.goods_content, _this, 0);
    }

    // 商品价格/划线价/库存
    data.item_sku_id = data.detail.sku==[] ? 1 :  data.detail.sku[0].key;
    var goods_price = data.detail.sku==[] ? data.detail.shop_price * (data.detail.discount ? data.detail.discount/ 10 : 1) : data.detail.sku[0].shop_price * (data.detail.discount ? data.detail.discount / 10 : 1);
	data.goods_price = goods_price.toFixed(2);
    data.stock_num = data.detail.sku==[] ? data.detail.store_count : data.detail.sku[0].store_count;
    // 初始化商品多规格
	
    if (data.detail.spec_type != 0) {
      data.specData = _this.initManySpecData(data.detail.spec_rel);
    }
    return data;
  },

  /**
   * 初始化商品多规格
   */
  initManySpecData: function(data) {
    for (let i in data) {
      for (let j in data[i].spec_item) {
        if (j < 1) {
          data[i].spec_item[0].checked = true;
          this.goods_spec_arr[i] = data[i].spec_item[0].id;
        }
      }
    }
    return data;
  },

  /**
   * 点击切换不同规格
   */
  modelTap: function(e) {
    let attrIdx = e.currentTarget.dataset.attrIdx,
      itemIdx = e.currentTarget.dataset.itemIdx,
      specData = this.data.specData;
    for (let i in specData) {
      for (let j in specData[i].spec_item) {
        if (attrIdx == i) {
          specData[i].spec_item[j].checked = false;
          if (itemIdx == j) {
            specData[i].spec_item[itemIdx].checked = true;
            this.goods_spec_arr[i] = specData[i].spec_item[itemIdx].id;
          }
        }
      }
    }
    this.setData({
      specData
    });
    // 更新商品规格信息
    this.updateSpecGoods();
  },

  /**
   * 更新商品规格信息
   */
  updateSpecGoods: function() {
	 
    let spec_sku_id = this.goods_spec_arr.join('_');
    // 查找skuItem
    let spec_list = this.data.detail.sku,
      skuItem = spec_list.find((val) => {
        return val.key == spec_sku_id;
      });
    // 记录item_sku_id
    // 更新商品价格、划线价、库存
    if (typeof skuItem === 'object') {
      this.setData({
        item_sku_id: skuItem.key,
        goods_price: skuItem.shop_price*this.data.detail.discount / 10,
        line_price: skuItem.price,
        stock_num: skuItem.store_count,
      });
    }else{
	  this.setData({
		stock_num:0
	  })
	}
  },

  /**
   * 设置轮播图当前指针 数字
   */
  setCurrent: function(e) {
    this.setData({
      currentIndex: e.detail.current + 1
    });
  },

  /**
   * 控制商品规格/数量的显示隐藏
   */
  onChangeShowState: function() {
    this.setData({
      showView: !this.data.showView
    });
  },

  /**
   * 返回顶部
   */
  goTop: function(t) {
    this.setData({
      scrollTop: 0
    });
  },

  /**
   * 显示/隐藏 返回顶部按钮
   */
  scroll: function(e) {
    this.setData({
      floorstatus: e.detail.scrollTop > 200
    })
  },

  /**
   * 增加商品数量
   */
  up: function() {
    this.setData({
      goods_num: ++this.data.goods_num
    })
  },

  /**
   * 减少商品数量
   */
  down: function() {
    if (this.data.goods_num > 1) {
      this.setData({
        goods_num: --this.data.goods_num
      });
    }
  },

  /**
   * 跳转购物车页面
   */
  flowCart: function() {
    wx.switchTab({
      url: "../flow/index"
    });
  },

  /**
   * 加入购物车and立即购买
   */
  submit: function(e) {
    let _this = this,
      submitType = e.currentTarget.dataset.type;
	let prom = e.currentTarget.dataset.prom;
	let other = e.currentTarget.dataset.other;
	other ? other :other = 0;
	prom ? prom :prom = 0;
    if (submitType === 'buyNow') {
      // 立即购买
      wx.navigateTo({
        url: '../flow/checkout?' + App.urlEncode({
          order_type: 'buyNow',
          item_id: _this.data.item_id,
          goods_num: _this.data.goods_num,
          item_sku_id: _this.data.item_sku_id,
		  prom: prom,
		  other: other
        })
      });
    } else if (submitType === 'addCart') {
      // 加入购物车
      App._post_form('cart/add', {
        item_id: _this.data.item_id,
        goods_num: _this.data.goods_num,
        item_sku_id: _this.data.item_sku_id,
      }, function(result) {
        App.showSuccess(result.msg);
        _this.setData(result.data);
      });
    }
  },

  /**
   * 浏览商品图片
   */
  previewImages: function(e) {
    let index = e.currentTarget.dataset.index,
      imageUrls = [];
    this.data.detail.image.forEach(function(item) {
      imageUrls.push(item.file_path);
    });
    wx.previewImage({
      current: imageUrls[index],
      urls: imageUrls
    })
  },

  /**
   * 跳转到评论
   */
  navigateToComment: function() {
    wx.navigateTo({
      url: './comment/comment?item_id=' + this.data.item_id
    })
  },

  /**
   * 分享当前页面
   */
  onShareAppMessage: function() {
    // 构建页面参数
    let params = App.urlEncode({
      'item_id': this.data.item_id,
      'referee_id': App.getUserId()
    });
    return {
      title: this.data.detail.name,
      desc: "",
      path: "/pages/goods/index?" + params
    };
  },

  /**
   * 显示分享选项
   */
  openActionsheet(e) {
    // 记录formId
    App.saveFormId(e.detail.formId);
    this.setData({
      'share.show': true
    });
  },

  /**
   * 关闭分享选项
   */
  closeActionSheet() {
    this.setData({
      'share.show': false
    });
  },

  /**
   * 点击分享选项
   */
  clickAction(e) {
    if (e.detail.index === 0) {
      // 显示商品海报
      this.showPoster();
    }
    this.closeActionSheet();
  },

  /**
   * 切换商品海报
   */
  togglePopup() {
    this.setData({
      'share.showPopup': !this.data.share.showPopup
    });
  },

  /**
   * 显示商品海报图
   */
  showPoster: function() {
    let _this = this;
    wx.showLoading({
      title: '加载中',
    });
    App._get('goods/poster', {
      item_id: _this.data.item_id
    }, function(result) {
      _this.setData(result.data, function() {
        _this.togglePopup();
      });
    }, null, function() {
      wx.hideLoading();
    });
  },

  /**
   * 保存海报图片
   */
  savePoster: function(e) {
    let _this = this;
    // 记录formId
    App.saveFormId(e.detail.formId);
    wx.showLoading({
      title: '加载中',
    });
    // 下载海报图片
    wx.downloadFile({
      url: _this.data.qrcode,
      success: function(res) {
        wx.hideLoading();
        // 图片保存到本地
        wx.saveImageToPhotosAlbum({
          filePath: res.tempFilePath,
          success: function(data) {
            wx.showToast({
              title: '保存成功',
              icon: 'success',
              duration: 2000
            });
            // 关闭商品海报
            _this.togglePopup();
          },
          fail: function(err) {
            if (err.errMsg === 'saveImageToPhotosAlbum:fail auth deny') {
              wx.showToast({
                title: "请允许访问相册后重试",
                icon: "none",
                duration: 1000
              });
              setTimeout(function() {
                wx.openSetting();
              }, 1000);
            }
          },
          complete(res) {
            console.log('complete');
            // wx.hideLoading();
          }
        })
      }
    })
  },
  

})